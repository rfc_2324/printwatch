with import <nixpkgs> {};
#{  pkgs.ib
#, pkgs.stdenv
#, pkgs.dbus
#, pkgs.desktop-file-utils
#, pkgs.glib
#, pkgs.gnome
#, pkgs.gobject-introspection
#, pkgs.gsettings-desktop-schemas
#, pkgs.gtk3
#, pkgs.libhandy
#, pkgs.meson
#, pkgs.cmake
#, pkgs.ninja
#, pkgs.pkg-config
#, pkgs.python3
#, pkgs.wrapGAppsHook
#}:

stdenv.mkDerivation rec {
  name = "printwatch";
  outputs = [ "out" ];
  src = ./.;
  
  nativeBuildInputs = [ 
    desktop-file-utils
    meson 
    cmake
    ninja 
    pkg-config
    glib
    gobject-introspection
    wrapGAppsHook 
    (python3.withPackages (pkgs: with pkgs; [
      pygatt
      dbus-python
      requests
      pyxdg
      pygobject3
    ]))
    wrapGAppsHook
  ];
  
  buildInputs = [ 
    gtk3
    dbus
    glib
    gnome.adwaita-icon-theme
    gsettings-desktop-schemas
<<<<<<< HEAD
    #appstream-glib 
=======
    appstream-glib 
>>>>>>> dev-watch
    desktop-file-utils
    libhandy
  ];

  mesonFlags = [
  ];

  postPatch = ''
    chmod +x build-aux/meson/postinstall.py
    patchShebangs build-aux/meson/postinstall.py
  '';

  meta = with lib; {
    description = "printwatch";
    homepage = "https://gitlab.com/rfc_2324/printwatch.git";
    license = licenses.gpl3Plus;
    maintainers = [ "alexk" ];
    platforms = platforms.linux;
  };
}
