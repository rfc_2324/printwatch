# Printwatch :
## Introduction
This companion app for [InfiniTime](https://github.com/JF002/InfiniTime) devices connects to 3d printer firmware ([Klipper3d](https://github.com/Klipper3d/klipper)) via network and to an PineTime device via Bluetooth.
It is then possible to monitor the printstatus on the watch.

**You will need :**
1.  A 3d printer using Klipper3d firmware, usually found on [Voron](https://www.vorondesign.com) printers.
2.  A [PineTime](https://www.pine64.org/pinetime/) device running this version of [InfiniTime](https://gitlab.com/rfc_2324/InfiniTime/-/tree/pw1.3.0)
3.  And this companion app.

### It will then look like this :

![PinePhone and InfiniTime](doc/pictures/IMG_20210919_200239.jpg "PinePhone and InfiniTime")

### Please note:
I make this project to get into application development for PineTime devices. This is a very early version of this software, so expect things not to run smooth.

**Build flatpak for pinephone:**

If you are using code-oss:
In .vscode/tasks.json are predefined tasks to build a flatpak bundle

```
$ flatpak-builder --arch=aarch64 --repo=repo --force-clean .flatpak at.alexk.printwatch.json
$ flatpak build-bundle --arch=aarch64 ./repo/ .flatpak/out/printwatch-aarch64.flatpak at.alexk.printwatch
```
