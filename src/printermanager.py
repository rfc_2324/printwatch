from gi.repository import Gio, GObject
from .config import config
from threading import Timer

import gi
import requests
import json
import logging

gi.require_version('Gtk', '3.0')

logger = logging.getLogger(__name__)
# Set urllib log less verbose
urllib_log = logging.getLogger('urllib3')
urllib_log.setLevel(logging.INFO)

moonraker_api = {
    "list_objects": "/printer/objects/list",
    "host_info": "/printer/info",
    "list_sensors": "/printer/objects/query?heaters=available_sensors",
    "query_extruder": "/printer/objects/query?extruder=temperature",
    "query_bed": "/printer/objects/query?heater_bed=temperature",
    "print_stats": "/printer/objects/query?print_stats",
    "query_progress": "/printer/objects/query?display_status=progress"
}


class PrinterManagerTask():
    """A Timer class that does not stop, unless you want it to."""

    def __init__(self, seconds, target):
        self._should_continue = False
        self.is_running = False
        self.seconds = seconds
        self.target = target
        self.thread = None

    def _handle_target(self):
        self.is_running = True
        self.target()
        self.is_running = False
        self._start_timer()

    def _start_timer(self):
        # Code could have been running when cancel was called.
        if self._should_continue:
            self.thread = Timer(self.seconds, self._handle_target)
            self.thread.setDaemon(True)
            self.thread.start()

    def start(self):
        if not self._should_continue and not self.is_running:
            self._should_continue = True
            self._start_timer()
        else:
            print("Timer already started or running, please wait.")

    def cancel(self):
        if self.thread is not None:
            # Just in case thread is running and cancel fails.
            self._should_continue = False
            self.thread.cancel()
        else:
            print("Timer never started or failed to initialize.")


class PrinterManager(GObject.GObject):
    """Manage connectivity to Moonraker
    TODO: set url just if reachable
    """

    def __init__(self, my_url):
        """Class init function

        Args:
            my_url (str): The base url part without 'http://' and query
        """
        logger.debug("PrinterManager __init__")

        GObject.GObject.__init__(self)

        self.api = moonraker_api
        self.config = config()
        self.base_url = my_url
        self.last_printer = self.config.get_property_j("printer-last-used")
        self.printer_set = set()
        self.last_data = dict()

        # Task start and cancel called from window.py
        self._seconds = 5
        self.timer = PrinterManagerTask(self._seconds, self._on_timer)

        try:
            GObject.signal_new('printer-data-received',
                               self,
                               GObject.SIGNAL_RUN_LAST,
                               GObject.TYPE_NONE,
                               (),
                               )
        except RuntimeError:
            logger.debug("Signal exists")

    # def __del__(self):
    #     logger.debug("PrinterManager __del__")
    #     try:
    #         self.timer.cancel()
    #     except AttributeError:
    #         # Timer not instantiated
    #         pass

    # def __delete__(self, instance):
    #     logger.debug("PrinterManager __delete__")
    #     try:
    #         self.timer.cancel()
    #     except AttributeError:
    #         # Timer not instantiated
    #         pass

    def _on_timer(self):
        # logger.debug("Timer tick!")
        d = self.get_printer_object()
        if d is not None:
            self.emit("printer-data-received")

    def _save_last_printer(self, ip):
        """write the ip to config file

        Args:
            ip (str): The ip address to write
        """
        logger.debug("_save_last_printer")
        self.config.set_property_j("printer-last-used", ip)
        self.printer_set.add(ip)
        logger.debug("printer_set ({})".format(self.printer_set))
        self.config.append_property_j("printers", ip)

    def _make_request(self, query):
        """Request data from printhost

        Args:
            query (url): HTTP request

        Returns:
            string: The response or None
        """
        try:
            response = requests.get(query, timeout=2.0)
        except requests.ConnectionError as e:
            logger.debug("ConnectionError: ({})".format(e))
            return None
        except requests.Timeout as e:
            logger.debug("Timeout: ({})".format(e))
            return None
        except requests.exceptions.HTTPError as e:
            logger.debug("HTTPError: ({})".format(e))
            return None
        except requests.ConnectTimeout as e:
            logger.debug("ConnectTimeout: ({})".format(e))
            return None

        if response.status_code == requests.codes.ok:
            return self.load_json(response)
        else:
            logger.warning("Status: ({})".format(response.status_code))
            return None

    def get_last_printer(self):
        return self.last_printer

    def get_printers(self):
        return self.printer_set

    def get_network_state(self):
        """Prints the current network state
        """
        monitor = Gio.NetworkMonitor.get_default()

        if monitor.get_network_available():
            return(True)
        else:
            return(False)

    def get_printer_object(self):
        """Returns the query response from Moonraker
        see: https://moonraker.readthedocs.io/en/stable/web_api/

        Returns:
            dict: Printer data or None
        """

        data = list()

        query = "http://" + self.base_url + self.api["host_info"]
        response = self._make_request(query)
        if response is None:
            return None
        else:   # The rest of the requests just if ok
            data.append(response["result"]["hostname"])
            data.append(response["result"]["state_message"])
            data.append(response["result"]["software_version"])

        query = "http://" + self.base_url + self.api["query_extruder"]
        response = self._make_request(query)
        if response is not None:
            data.append(
                response["result"]["status"]["extruder"]["temperature"]
            )

        query = "http://" + self.base_url + self.api["query_bed"]
        response = self._make_request(query)
        if response is not None:
            data.append(
                response["result"]["status"]["heater_bed"]["temperature"]
            )

        query = "http://" + self.base_url + self.api["print_stats"]
        response = self._make_request(query)
        if response is not None:
            data.append(
                response["result"]["status"]["print_stats"]["print_duration"]
            )
            data.append(
                response["result"]["status"]["print_stats"]["total_duration"]
            )
            data.append(response["result"]["status"]["print_stats"]["state"])

        query = "http://" + self.base_url + self.api["query_progress"]
        response = self._make_request(query)
        if response is not None:
            data.append(
                response["result"]["status"]["display_status"]["progress"]
            )

        if len(data) > 0:
            if not data[4]:
                logger.debug("No bed data.")
                data[4] = 0

            d = {
                "hostname": data[0],  # string
                "hosturl": self.base_url,  # not used by watch
                "host_state": data[1],  # not used by watch
                "version": data[2],  # not used by watch
                "extruder": int(data[3]),  # int()
                "bed": int(data[4]),  # int()
                "print_duration": data[5],  # not used by watch
                "total_duration": data[6],  # not used by watch
                "print_state": data[7],  # not used by watch
                "print_progress": int(data[8] * 100)  # lvgl needs int 0 - 100
            }
            self.last_data = d
            # logger.debug("progress: ({})".format(d["print_progress"]))
            return d
        else:
            return None

    def load_json(self, r):
        try:
            j = json.loads(r.text)
        except json.JSONDecodeError as e:
            logger.warning("Json error: ({})".format(e.msg))
            return None
        else:
            return j

    def set_printer(self, base_url):
        """Update printer url

        Args:
            base_url (str): The ip or hostname of the printer
        """
        logger.debug("set_printer: ({})".format(base_url))
        self.base_url = base_url
        self.last_printer = base_url
        self._save_last_printer(base_url)
