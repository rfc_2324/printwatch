# window.py
#
# Copyright 2021 alexK
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, Pango, Handy
from .printermanager import PrinterManager
from .watchmanager import (
    InfiniTimeDevice,
    BluetoothDisabled,
    NoAdapterFound,
)
from .config import config

import logging
import subprocess

# Debug output to console
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


@Gtk.Template(resource_path="/at/alexk/printwatch/window.ui")
class PrintwatchWindow(Gtk.ApplicationWindow):
    """The main window class

    Args:
        Gtk (ApplicationWindow): The window template
    """

    __gtype_name__ = "PrintwatchWindow"

    Handy.init()

    # Declare the used objects from the ui template
    main_stack = Gtk.Template.Child()
    main_page = Gtk.Template.Child()
    printer_listbox_main = Gtk.Template.Child()
    printer_listbox = Gtk.Template.Child()
    printer_placeholder = Gtk.Template.Child()
    show_printer_page_btn = Gtk.Template.Child()
    watch_listbox = Gtk.Template.Child()
    watch_listbox_main = Gtk.Template.Child()
    show_watch_page_btn = Gtk.Template.Child()
    printer_page = Gtk.Template.Child()
    printer_bottom_lbl = Gtk.Template.Child()
    printer_add_popover = Gtk.Template.Child()
    printer_entry = Gtk.Template.Child()
    printer_icon = Gtk.Template.Child()
    printer_spinner = Gtk.Template.Child()
    watch_page = Gtk.Template.Child()
    watch_add_popover = Gtk.Template.Child()
    watch_entry = Gtk.Template.Child()
    watch_open_bt_settings = Gtk.Template.Child()
    watch_open_bt_settings_btn = Gtk.Template.Child()
    watch_bottom_lbl = Gtk.Template.Child()
    watch_icon = Gtk.Template.Child()
    watch_spinner = Gtk.Template.Child()
    go_back_btn = Gtk.Template.Child()
    list_add_btn = Gtk.Template.Child()
    data_icon = Gtk.Template.Child()

    def __init__(self, **kwargs):
        """class init"""
        self.config = config()
        self.printer_manager = None
        # self.watch_manager = None
        # self.printer_signal_id = None
        self.printer_url = None
        self.watch = None
        self.watch_mac = None
        self.watchpage_visible = False
        self.printerpage_visible = False
        self.url_for_row = dict()
        self.mac_for_row = dict()

        super().__init__(**kwargs)

    def add_printer(self):
        """Show the printer popover"""
        logger.debug("add_printer")
        self.printer_add_popover.set_visible(True)

    def add_watch(self, mac=None):
        """Scan for InfiniTime device"""
        # logger.debug("add_watch")

        self.watch_spinner.set_visible(True)
        self.watch_spinner.start()

        watch = InfiniTimeDevice(mac)
        if watch.device is not None:
            watch.scan_device()
            if watch.has_printservice:
                # self.watch_mac = mac
                row = self.make_listbox_row(("Mac:", mac),
                                            ("Note:", ""),
                                            ("Printservice", "available")
                                            )
                self.mac_for_row[row] = mac
                self.watch_listbox.add(row)
            else:
                row = self.make_listbox_row(("Mac:", mac),
                                            ("Note:", ""),
                                            ("Printservice", "not available"))
                self.watch_listbox.add(row)
                row.set_sensitive(False)
            watch.device.disconnect()
        else:
            known_watches = self.config.get_property_j("watches")
            for w in known_watches:
                row = self.make_listbox_row(("Mac:", w["mac"]),
                                            ("Note:", w["alias"]),
                                            ("Printservice", "unknown"))
                self.mac_for_row[row] = w["mac"]
                self.watch_listbox.add(row)
            self.watch_bottom_lbl.set_text("Is BT enabled?")

        self.watch_spinner.stop()

    def make_listbox_row(self, val1, val2, val3) -> Gtk.ListBoxRow:
        """Update the view with the recieved data

        Args:
            val1 (dict): Box data
            val2 (dict): Box data
            val3 (dict): Box data
        Returns:
            Gtk.ListBoxRow: The row to insert
        """
        logger.debug("make_listbox_row.")
        row = Gtk.ListBoxRow()
        grid = Gtk.Grid()
        grid.set_hexpand(True)
        grid.set_row_spacing(2)
        grid.set_column_spacing(6)
        grid.set_margin_top(2)
        grid.set_margin_bottom(2)
        grid.set_margin_left(2)
        grid.set_margin_right(2)
        row.add(grid)

        label_val1 = Gtk.Label(label=val1[0], xalign=1.0)
        label_val1.get_style_context().add_class("dim-label")
        grid.attach(label_val1, 1, 0, 1, 1)
        value_val1 = Gtk.Label(label=val1[1], xalign=0.0)
        value_val1.set_hexpand(True)
        grid.attach(value_val1, 2, 0, 1, 1)

        label_val2 = Gtk.Label(label=val2[0], xalign=1.0)
        label_val2.get_style_context().add_class("dim-label")
        grid.attach(label_val2, 3, 0, 1, 1)
        value_val2 = Gtk.Label(label=val2[1], xalign=0.0)
        value_val2.set_ellipsize(Pango.EllipsizeMode.START)
        value_val2.set_hexpand(True)
        grid.attach(value_val2, 4, 0, 1, 1)

        label_val3 = Gtk.Label(label=val3[0], xalign=1.0)
        label_val3.get_style_context().add_class("dim-label")
        grid.attach(label_val3, 1, 1, 1, 1)
        value_val3 = Gtk.Label(label=val3[1], xalign=0.0)
        value_val3.set_ellipsize(Pango.EllipsizeMode.END)
        value_val3.set_hexpand(True)
        grid.attach(value_val3, 2, 1, 1, 1)

        row.show_all()

        return row

    def destroy_manager(self):
        """Stop the watch manager and cleanup"""
        logger.debug(("destroy_manager"))
        # if self.watch_manager:
        #     if self.watch is not None:
        #         self.watch.disconnect()
        #     self.watch_manager.stop()
        #     self.watch_manager = None
        self.watch_icon.set_from_icon_name(
            "bluetooth-disconnected-symbolic", Gtk.IconSize.BUTTON
        )

    def on_printer_data_ready(self, data):
        """Send data to watch

        Args:
            data (None): Not used
        """
        logger.debug("on_printer_data_ready")
        self.last_printer_data = self.printer_manager.last_data
        if self.main_stack.get_visible_child_name() == "main_page":
            # TODO: NoneType error if no watch
            if self.watch.device is not None and self.watch.can_write:
                # self.watch.resolve_services()
                self.watch.send_printdata(self.last_printer_data)
                # self.watch_icon.set_from_icon_name(
                #     "bluetooth-active-symbolic", Gtk.IconSize.BUTTON
                # )
                self.data_icon.set_from_icon_name(
                    "network-transmit-symbolic", Gtk.IconSize.BUTTON
                )

    def on_watch_signal(self, *data):
        if data[1] == "data-sent":
            logger.debug("Watch data sent.")
            self.data_icon.set_from_icon_name("network-idle-symbolic",
                                              Gtk.IconSize.BUTTON)

    @Gtk.Template.Callback()
    def on_show_printer_page_btn_clicked(self, *data):
        """Navigate to the printer page"""
        logger.debug("on_show_printer_page_btn_clicked")
        self.main_stack.set_visible_child_name("printer_page")
        self.go_back_btn.set_visible(True)
        self.list_add_btn.set_visible(True)
        self.printerpage_visible = True
        last_ip = self.config.get_property_j("printer-last-used")

        if last_ip is not None:
            self.printer_spinner.set_visible(True)
            self.printer_spinner.start()
            printermanager = PrinterManager(last_ip)
            d = printermanager.get_printer_object()
            if d:
                row = self.make_listbox_row(
                    ("Host:", d["hostname"]),
                    ("Url:", d["hosturl"]),
                    ("FW:", d["version"]),
                )
                self.printer_listbox.add(row)
                self.url_for_row[row] = last_ip
            del printermanager
            self.printer_spinner.stop()
            self.printer_spinner.set_visible(False)

    @Gtk.Template.Callback()
    def on_show_watch_page_btn_clicked(self, *data):
        """Navigate to the watch page"""

        logger.debug("on_show_watch_page_btn_clicked")
        # Try to write to config
        w = {"alias": "Test", "mac": "aa:bb:cc:dd:ee:ff", "paired": False}
        self.config.append_property_j("watches", w)

        self.main_stack.set_visible_child_name("watch_page")
        self.go_back_btn.set_visible(True)
        self.list_add_btn.set_visible(True)
        self.watchpage_visible = True
        # Do we have a last used device
        last_watch = self.config.get_property_j("watch-last-used")
        if last_watch:
            logger.debug("Last watch ({})".format(last_watch))
            self.add_watch(last_watch)
        else:
            logger.debug("No last watch")

    @Gtk.Template.Callback()
    def on_go_back_button_clicked(self, *data):
        """Navigate back to the main page"""
        logger.debug("on_go_back_button_clicked")

        # Handling printer if on printer page
        if self.main_stack.get_visible_child_name() == "printer_page":
            # Restart PrinterManager with selected printer
            try:
                self.printer_manager.timer.cancel()
            except AttributeError:
                pass
            del self.printer_manager
            self.printer_manager = PrinterManager(self.printer_url)
            self.printer_manager.timer.start()
            self.printer_manager.connect(
                "printer-data-received", self.on_printer_data_ready
            )
            self.printer_manager.set_printer(self.printer_url)
            # Add a row to the main listbox
            d = self.printer_manager.get_printer_object()
            if d:
                row = self.make_listbox_row(
                    ("Host:", d["hostname"]),
                    ("Url:", d["hosturl"]),
                    ("FW:", d["version"]),
                )
                self.printer_listbox_main.add(row)
                self.url_for_row[row] = self.printer_url
                self.printer_icon.set_from_icon_name(
                    "network-wireless-signal-excellent-symbolic",
                    Gtk.IconSize.BUTTON
                )

        # Handling watch if on watch page
        elif self.main_stack.get_visible_child_name() == "watch_page":
            self.watch = InfiniTimeDevice(mac_address=self.watch_mac)
            if self.watch is None:
                self.watch_bottom_lbl.set_text("Error! Is BT enabled?")
                return

            if not self.watch.resolve_services():
                self.watch_bottom_lbl.set_text("Error resolving services.")
                return
            logger.info("Device ({}) connected.".format(
                self.watch.addr()))

            # self.config.set_property("watch-last-used", self.watch.addr())
            self.config.set_property_j("watch-last-used", self.watch.addr())

            self.watch.connect(
                "watch-signal", self.on_watch_signal
            )

            for c in self.watch_listbox_main.get_children():
                self.watch_listbox_main.remove(c)
            row = self.make_listbox_row(
                ("Watch:", self.watch.manufact.decode('utf-8')),
                ("Fw:", self.watch.firmware.decode('utf-8')),
                ("Mac:", self.watch.addr()),
            )
            self.watch_listbox_main.add(row)
            self.mac_for_row[row] = self.watch.addr()
            self.watch_icon.set_from_icon_name(
                "bluetooth-active-symbolic", Gtk.IconSize.BUTTON
            )

        self.main_stack.set_visible_child_name("main_page")
        self.go_back_btn.set_visible(False)
        self.list_add_btn.set_visible(False)

    @Gtk.Template.Callback()
    def on_list_add_btn_clicked(self, *data):
        """Show the add printer or watch popover"""
        logger.debug("on_list_add_btn_clicked")
        if self.main_stack.get_visible_child_name() == "printer_page":
            logger.debug("Show add printer popover")
            # Process add printer
            self.add_printer()
        elif self.main_stack.get_visible_child_name() == "watch_page":
            logger.debug("Show add watch popover")
            self.watch_add_popover.set_visible(True)
        else:
            logger.warning("No popover")

    @Gtk.Template.Callback()
    def on_printer_entry_icon_press(self, *data):
        """Create a printer ListBoxRow"""
        logger.debug("on_printer_entry_icon_press")
        url = self.printer_entry.get_text()
        # Do we have the url already
        if url != "" and url not in self.url_for_row.values():
            self.printer_url = url
            logger.debug("Url: ({})".format(self.printer_url))

            self.printer_spinner.set_visible(True)
            self.printer_spinner.start()

            # Create a PrinterManager for connection check
            printer_manager = PrinterManager(self.printer_url)
            pdata = printer_manager.get_printer_object()
            # Returned printer data not valid
            if pdata is None:
                logger.warning("Returned printer data not valid")
                row = self.make_listbox_row(
                    ("Host:", "Offline"),
                    ("Url:", self.printer_url),
                    ("FW:", "n/a")
                )
                self.url_for_row[row] = self.printer_url
                # TODO: row.set_sensitive(False)
            else:
                logger.debug(
                    "Host: ({}), @ ({}), FW: ({})".format(
                        pdata["hostname"], self.printer_url, pdata["version"]
                    )
                )
                row = self.make_listbox_row(
                    ("Host:", pdata["hostname"]),
                    ("Url:", self.printer_url),
                    ("FW:", pdata["version"]),
                )
                self.url_for_row[row] = self.printer_url
                row.set_sensitive(True)

            self.printer_spinner.stop()
            self.printer_spinner.set_visible(False)
            self.printer_listbox.add(row)
            self.printer_add_popover.set_visible(False)

            del printer_manager

        else:
            logger.warning("No url provided.")
            self.printer_bottom_lbl.set_text("No valid printer url.")
            self.printer_add_popover.set_visible(False)

    @Gtk.Template.Callback()
    def on_watch_entry_icon_press(self, *data):
        logger.debug("on_watch_entry_icon_press")
        mac = self.watch_entry.get_text()
        self.watch_add_popover.set_visible(False)
        self.watch_spinner.start()
        if mac != "" and mac not in self.mac_for_row.values():
            self.add_watch(mac)

    @Gtk.Template.Callback()
    def on_printer_listbox_row_activated(self, *data):
        """Select printer

        Args:
            tuple: Gtk.ListBox, GtkListBoxRow
        """
        # logger.debug("on_printer_listbx_row_activated")
        self.printer_url = self.url_for_row[data[1]]
        logger.debug("New printer url ({})".format(self.printer_url))
        self.printer_bottom_lbl.set_text("Selected: {}".format(
            self.printer_url))
        self.printer_placeholder.set_text("Waiting for data")

    @Gtk.Template.Callback()
    def on_watch_listbox_row_activated(self, *data):
        """Select watch

        Args:
            tuple: Gtk.ListBox, GtkListBoxRow
        """
        # logger.debug("on_watch_listbox_row_activated")
        self.watch_mac = self.mac_for_row[data[1]]
        logger.info("Using watch mac ({})".format(self.watch_mac))
        self.watch_bottom_lbl.set_text("Selected: {}".format((self.watch_mac)))

    @Gtk.Template.Callback()
    def on_watch_open_bt_settings_btn_clicked(self, *data):
        logger.debug("on_watch_open_bt_settings_btn_clicked")
        subprocess.Popen(["gnome-control-center", "bluetooth"])
        self.watch_open_bt_settings.set_visible(False)

    @Gtk.Template.Callback()
    def on_PrintwatchWindow_destroy(self, *args):
        """Cleanup"""
        logger.debug("on_PrintwatchWindow_destroy")
        try:
            self.printer_manager.timer.cancel()
        except AttributeError:
            # Timer not instantiated
            pass
