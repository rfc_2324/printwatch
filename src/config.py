import xdg.BaseDirectory
import json
from pathlib import Path


class config:
    """Save and restore settings
    """
    default_config_json = {
        "printers": (
            "192.168.10.56",
            "192.168.10.209"
        ),
        "printer-last-used": "None",
        "watches": [
            {
                "alias": "InfiniTime",
                "mac": "fe:3e:56:22:23:f7",
                "paired": "False"
            },
            {
                "alias": "DevKit",
                "mac": "f6:8d:a2:08:1c:90",
                "paired": "False"
            }
        ],
        "watch-last-used": "None"
    }

    config_dir = xdg.BaseDirectory.xdg_config_home
    config_file_j = config_dir + "/printwatch.json"

    def create_config_file_j(self):
        """Create config file and store defaults
        """
        if not Path(self.config_dir).is_dir():
            Path.mkdir(Path(self.config_dir))
        # if config file is not valid, load defaults
        if not self.file_valid_j():
            with open(self.config_file_j, "w") as f:
                json.dump(self.default_config_json, f, indent=2)

    def file_valid_j(self):
        """Check if the config file has proper entries

        Returns:
            bool: True if config file is valid, else False
        """
        try:
            with open(self.config_file_j) as f:
                config = json.load(f)
        except IOError:
            return False

        for key in config:
            if key not in self.default_config_json:
                return False
        return True

    def get_property_j(self, key):
        with open(self.config_file_j) as f:
            config = json.load(f)
        prop = config[key]
        return prop

    def set_property_j(self, key, val):
        with open(self.config_file_j, 'r') as f:
            config = json.load(f)
        config[key] = val
        with open(self.config_file_j, 'w') as f:
            json.dump(config, f, indent=2)

    def append_property_j(self, key, val):
        with open(self.config_file_j, 'r') as f:
            config = json.load(f)
        if val not in config[key]:
            config[key].append(val)
            with open(self.config_file_j, 'w') as f:
                json.dump(config, f, indent=2)
