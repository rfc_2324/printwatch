from .config import config
import logging
from gi.repository import GObject
from bluepy.btle import (
    BTLEException,
    Peripheral,
)

logger = logging.getLogger(__name__)

BTSVC_INFO = "0000180a-0000-1000-8000-00805f9b34fb"
BTSVC_ALERT = "00001811-0000-1000-8000-00805f9b34fb"
BTSVC_PRINT = "00020000-78fc-48fe-8e23-433b3a1942d0"
BTCHAR_MANUFACTURER = "00002a29-0000-1000-8000-00805f9b34fb"
BTCHAR_FIRMWARE = "00002a26-0000-1000-8000-00805f9b34fb"
BTCHAR_NEWALERT = "00002a46-0000-1000-8000-00805f9b34fb"
BTCHAR_PRINTHOST = "00020001-78fc-48fe-8e23-433b3a1942d0"
BTCHAR_PRINTETEMP = "00020002-78fc-48fe-8e23-433b3a1942d0"
BTCHAR_PRINTBTEMP = "00020003-78fc-48fe-8e23-433b3a1942d0"
BTCHAR_PRINTPROGRESS = "00020004-78fc-48fe-8e23-433b3a1942d0"
# BTCHAR_PRINTDURATION = "00020005-78fc-48fe-8e23-433b3a1942d0"
BTCHAR_PRINTSTATUS = "00020005-78fc-48fe-8e23-433b3a1942d0"


class InfiniTimeDevice(GObject.GObject):
    """Class that represents an InfiniTime watch

    Args:
        GObject (GObject): Needed for signals
    """

    def __init__(self, mac_address=None):

        GObject.GObject.__init__(self)

        self.config = config()
        self.device = None
        self.mac = mac_address
        self.device_set = set()
        self.aliases = dict()
        self.has_printservice = False
        self.can_write = False

        try:
            GObject.signal_new(
                "watch-signal",
                self,
                GObject.SIGNAL_RUN_LAST,
                GObject.TYPE_PYOBJECT,
                (GObject.TYPE_PYOBJECT,),
            )
        except RuntimeError:
            logger.debug("Signal exists")

        self.init()

    def init(self) -> Peripheral:
        """Initialize watch

        Returns:
            Peripheral: A device object or None
        """
        try:
            self.device = Peripheral(self.mac, "random")
        except (BTLEException, ValueError) as e:
            logger.debug("Watch init failed ({})".format(e))
            return None
        logger.info("Info ({})".format(self.device.getState()))
        return self.device

    def addr(self) -> str:
        """Return the mac address

        Returns:
            str: The device object mac address
        """
        return self.device.addr

    def disconnect(self):
        """Disconnect the device
        """
        self.device.disconnect()

    def scan_device(self):
        """Scan for printservice
        """
        logger.debug("Scanning %s (%s)" % (
            self.device.addr, self.device.addrType)
        )
        try:
            pservice = self.device.getServiceByUUID(BTSVC_PRINT)
        except BTLEException as e:
            logger.debug(("Printservice: ({})".format(e)))
            self.has_printservice = False
            return

        logger.debug(("Printservice: ({})".format(pservice.uuid)))
        self.has_printservice = True

    def resolve_services(self) -> bool:
        """Map services to handles
        """
        logger.debug("services_resolved")

        if self.device is None:
            return False

        infosvc = None
        alertsvc = None
        printsvc = None

        for svc in self.device.getServices():
            if svc.uuid == BTSVC_INFO:
                infosvc = svc
            elif svc.uuid == BTSVC_ALERT:
                alertsvc = svc
            elif svc.uuid == BTSVC_PRINT:
                printsvc = svc

        self.manufact = b"n/a"
        self.firmware = b"n/a"
        if infosvc:
            man = infosvc.getCharacteristics(
                BTCHAR_MANUFACTURER
            )
            self.manufact = man[0].read()
            firm = infosvc.getCharacteristics(
                BTCHAR_FIRMWARE
            )
            self.firmware = firm[0].read()

        if alertsvc:
            self.alert_handle = alertsvc.getCharacteristics(
                BTCHAR_NEWALERT
            )

        if printsvc:
            logger.info("Printservice detected")
            self.can_write = True
            for c in printsvc.getCharacteristics():
                logger.info("c: ({})".format(c.uuid))
                if c.uuid == BTCHAR_PRINTHOST:
                    self.host_handle = c.getHandle()
                elif c.uuid == BTCHAR_PRINTETEMP:
                    self.etemp_handle = c.getHandle()
                elif c.uuid == BTCHAR_PRINTBTEMP:
                    self.btemp_handle = c.getHandle()
                elif c.uuid == BTCHAR_PRINTPROGRESS:
                    self.progress_handle = c.getHandle()
                elif c.uuid == BTCHAR_PRINTSTATUS:
                    self.status_handle = c.getHandle()

        return True

    def send_notification(self, alert_dict):
        """Send a notivication to the device

        Args:
            alert_dict (dict): The message dict
        """
        message = alert_dict["message"]
        alert_category = "0"  # simple alert
        alert_number = "0"  # 0-255
        title = alert_dict["sender"]
        msg = (
            str.encode(alert_category)
            + str.encode(alert_number)
            + str.encode("\0")
            + str.encode(title)
            + str.encode("\0")
            + str.encode(message)
        )

        # arr = bytearray(message, "utf-8")
        # self.new_alert_characteristic.write_value(arr)
        self.new_alert.write_value(msg)

    def send_printdata(self, data_dict):
        """This sends the data via bluetooth

        Args:
            data_dict (dict): Data to send defined in network.py
        """
        self.can_write = False
        logger.debug("watch can_write False")

        # 00020001-78fc-48fe-8e23-433b3a1942d0
        dat = str.encode(data_dict["hostname"])
        logger.debug("dat: ({}): ".format(dat))
        self.device.writeCharacteristic(self.host_handle, dat)

        # 00020002-78fc-48fe-8e23-433b3a1942d0
        dat = str.encode(str(data_dict["extruder"]))
        logger.debug("dat: ({}): ".format(dat))
        self.device.writeCharacteristic(self.etemp_handle, dat)

        # 00020003-78fc-48fe-8e23-433b3a1942d0
        dat = str.encode(str(data_dict["bed"]))
        logger.debug("dat: ({}): ".format(dat))
        self.device.writeCharacteristic(self.btemp_handle, dat)

        # 00020004-78fc-48fe-8e23-433b3a1942d0
        dat = str.encode(str(data_dict["print_progress"]))
        logger.debug("dat: ({}): ".format(dat))
        self.device.writeCharacteristic(self.progress_handle, dat)

        dat = (str.encode(str(data_dict["print_state"])))
        logger.debug("dat: ({}): ".format(dat))
        self.device.writeCharacteristic(self.status_handle, dat)

        self.can_write = True
        logger.debug("watch can_write True")
        self.emit("watch-signal", "data-sent")


class BluetoothDisabled(Exception):
    pass


class NoAdapterFound(Exception):
    pass


class BTLEDisconnectError(Exception):
    logger.debug("Device disconnected")
